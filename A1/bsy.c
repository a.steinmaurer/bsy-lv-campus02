#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define WELCOME_SCREEN "--- Willkommen zu BSY1 ---\n"
#define OPTION_SCREEN "Waehle eine Aktion im Programm aus: (q, a, c, f, p, n, e)\n"
#define SIG_MESSAGE "Nicht beenden!\n"
#define F_NAME "sys"

char arr[] = 
{
  0x47, 0x75, 0x74, 0x65, 0x73, 0x20,
  0x6e, 0x65, 0x75, 0x65, 0x73, 0x20,
  0x4a, 0x61, 0x68, 0x72, 0x21, 0x20,
  0x3a, 0x2d, 0x29, 0x0a 
};

FILE *fp;

void sigHandler(int sig);
void sigHandler2(int sig);

int main(int argc, char *argv[])
{
  int c;
  int tmp;
  char *mem;
  pid_t pid;

  printf(WELCOME_SCREEN);
  printf(OPTION_SCREEN);

  c = getchar();

  if(c == 'q')
  {
    signal(SIGINT, sigHandler);
    printf("Versuche den Prozess zu beenden!\n");
    while(1);
  }
  else if (c == 'a')
  {
    printf("Bei der Arbeit...\n");
    while(1)
    {
      printf(".");
      mem = calloc(512, sizeof(char));
      mem = NULL;
    }
   }
   else if(c == 'c')
   {
     printf("Warte auf Benutzereingabe: \n");
     while ((tmp = getchar()) != '\n' && tmp != EOF) { }
     tmp = getchar();

     while(1)
     {  
       printf("Die Eingabe war: %c\n", tmp);
       sleep(2);
     }
    }
    else if(c == 'f')
    {
      signal(SIGINT, sigHandler2);
      fp = fopen(F_NAME, "w");
      fputs(arr, fp);
      while(1);
    }
    else if(c == 'p')
    {
      pid = fork();
      if(pid == 0)
      {
        printf("Fast Fertig");
      } else {
        printf("Fertig");
      }
      while(1);
    } 
    else if(c == 'n')
    {
      pid = fork();
      if(pid == 0)
      {  
        execvp(argv[0], argv);
        while(1);
      } else {
        wait(&pid);
        while(1);
      }
    }
    else if(c == 'e')
    {
      return 0;
    } 
   return 0;
}

void sigHandler(int sig)
{
  write(STDOUT_FILENO, SIG_MESSAGE, 15);
}

void sigHandler2(int sig)
{
  fclose(fp);
}