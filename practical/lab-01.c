#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
  char input = '\0';
  input = getchar();
  pid_t pid;

  switch(input)
  {
    case 'a':
      pid = fork();
      if(pid == 0)
      {
        execvp(argv[0], argv);
      } else {
        wait(&pid);
      }
  }
  return 1;
}
