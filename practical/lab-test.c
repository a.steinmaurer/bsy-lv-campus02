#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define SIG_MESSAGE "This is not working!"

void signalHandler(int sig);

int main(int argc, char* argv[])
{
  int* memory = NULL;
  signal(SIGINT, signalHandler);
  while(1)
  {
    memory = calloc(1024, sizeof(char));
    memset((void*) memory, 1, 1024);
    memory = NULL;
  }
}

void signalHandler(int sig)
{
  write(STDOUT_FILENO, SIG_MESSAGE, 20);
}
